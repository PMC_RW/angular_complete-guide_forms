import { Component, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-template-driven-form',
  templateUrl: './template-driven-form.component.html',
  styleUrls: ['./template-driven-form.component.css'],
})
export class TemplateDrivenFormComponent {
  @ViewChild('f') signUpForm!: NgForm;
  answer: string = '';
  currencies: string[] = ['€ - Euro', '$ - Dollar', '£ - Pound'];
  user = { username: '', email: '', currency: '' };
  submitted: boolean = false;

  suggestUserName() {
    const suggestedName = 'Superuser';
    // set value of whole form
    // this.signUpForm.setValue({
    //   credentials: { username: suggestedName, email: '' },
    //   secret: 'pet',
    //   secretAnswer: '',
    //   currency: '€ - Euro',
    // });
    // set value of specific keys
    this.signUpForm.form.patchValue({
      credentials: { username: suggestedName },
    });
  }

  // onSubmit(form: NgForm) {
  //   console.log(form.value);
  // }

  // without passing "f" or accessing form before submitting
  onSubmit() {
    console.log(this.signUpForm);
    this.user.username = this.signUpForm.value.credentials.username;
    this.user.email = this.signUpForm.value.credentials.email;
    this.user.currency = this.signUpForm.value.currency;
    this.submitted = true;
    this.signUpForm.reset();
  }
}
