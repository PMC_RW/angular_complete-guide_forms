import { Component, ViewChild } from '@angular/core';

@Component({
  selector: 'app-td-form-assignment',
  templateUrl: './td-form-assignment.component.html',
  styleUrls: ['./td-form-assignment.component.css'],
})
export class TdFormAssignmentComponent {
  @ViewChild('subForm', { static: false }) subForm!: HTMLFormElement;
  showPassword: boolean = false;

  onShowPassword(): void {
    this.showPassword = true;
  }

  onHidePassword(): void {
    this.showPassword = false;
  }

  onSubmit() {
    console.log(this.subForm['value']);
    this.subForm.reset();
  }
}
