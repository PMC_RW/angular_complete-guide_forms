import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TdFormAssignmentComponent } from './td-form-assignment.component';

describe('TdFormAssignmentComponent', () => {
  let component: TdFormAssignmentComponent;
  let fixture: ComponentFixture<TdFormAssignmentComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [TdFormAssignmentComponent]
    });
    fixture = TestBed.createComponent(TdFormAssignmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
