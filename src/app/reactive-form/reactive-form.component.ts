import { Component, OnInit } from '@angular/core';
import {
  AbstractControl,
  AsyncValidatorFn,
  FormArray,
  FormControl,
  FormGroup,
  ValidationErrors,
  Validators,
} from '@angular/forms';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-reactive-form',
  templateUrl: './reactive-form.component.html',
  styleUrls: ['./reactive-form.component.css'],
})
export class ReactiveFormComponent implements OnInit {
  username: string = '';
  forbiddenUsernames = ['Chris', 'Tom', 'Cathrin'];
  email: string = '';
  country: string = 'Italy';
  animal: string = 'Chamois';
  colors: string[] = ['red', 'blue', 'green'];
  mamsl: number = 3500;

  reactiveForm!: FormGroup;
  credentials!: FormGroup;

  ngOnInit(): void {
    this.reactiveForm = new FormGroup({
      credentials: new FormGroup({
        username: new FormControl(null, [
          Validators.required,
          this.forbiddenNamesValidator.bind(this),
        ]),
        email: new FormControl(
          null,
          [Validators.required, Validators.email],
          this.forbiddenEmailsValidator()
        ),
      }),
      country: new FormControl(null, Validators.required),
      animals: new FormArray([]),
      color: new FormControl('blue'),
      mamsl: new FormControl(null),
    });
    // this.reactiveForm.valueChanges.subscribe((value) => console.log(value));
    this.reactiveForm.statusChanges.subscribe((value) => console.log(value));
    this.reactiveForm.setValue({
      credentials: {
        username: 'Max',
        email: 'max@test.com',
      },
      country: 'Texas',
      animals: [],
      color: 'red',
      mamsl: 2300,
    });
  }

  onAddAnimal(): void {
    const control = new FormControl(null, Validators.required);
    (<FormArray>this.reactiveForm.get('animals')).push(control);
  }

  get controls() {
    return (this.reactiveForm.get('animals') as FormArray).controls;
  }

  onSubmit(): void {
    console.log(this.reactiveForm.value);
    this.reactiveForm.reset();
  }

  forbiddenNamesValidator(
    control: FormControl
  ): { [s: string]: boolean } | null {
    if (this.forbiddenUsernames.indexOf(control.value) !== -1) {
      return { nameIsForbidden: true };
    }
    return null;
  }

  forbiddenEmailsValidator(): AsyncValidatorFn {
    return (control: AbstractControl): Promise<ValidationErrors | null> => {
      return new Promise((resolve, reject) => {
        setTimeout(() => {
          if (control.value === 'test@test.com') {
            resolve({ emailIsForbidden: true });
          } else {
            resolve(null);
          }
        }, 1500);
      });
    };
  }
}
