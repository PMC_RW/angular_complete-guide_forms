import { Component, OnInit } from '@angular/core';
import {
  AbstractControl,
  AsyncValidatorFn,
  FormControl,
  FormGroup,
  ValidationErrors,
  Validators,
} from '@angular/forms';

@Component({
  selector: 'app-reactive-form-assignment',
  templateUrl: './reactive-form-assignment.component.html',
  styleUrls: ['./reactive-form-assignment.component.css'],
})
export class ReactiveFormAssignmentComponent implements OnInit {
  projectForm!: FormGroup;

  ngOnInit(): void {
    this.projectForm = new FormGroup({
      projectName: new FormControl(null, [
        Validators.required,
        this.projectNameValidator,
      ]),
      email: new FormControl(
        null,
        [Validators.required, Validators.email],
        this.asyncEmailValidator()
      ),
      projectStatus: new FormControl('default'),
    });
  }

  projectNameValidator(control: FormControl): { [s: string]: boolean } | null {
    if (control.value === 'Test') {
      return { projectNameForbidden: true };
    }
    return null;
  }

  asyncEmailValidator(): AsyncValidatorFn {
    return (control: AbstractControl): Promise<ValidationErrors | null> => {
      return new Promise((resolve, reject) => {
        setTimeout(() => {
          if (control.value === 'test@test.com') {
            resolve({ emailIsForbidden: true });
          } else {
            resolve(null);
          }
        }, 1500);
      });
    };
  }

  onSubmit(): void {
    console.log(this.projectForm.value);
  }
}
